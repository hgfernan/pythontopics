#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
From int to float and vice-versa 

Created on Wed Jun 21 22:34:27 2023

@author: hilton
"""

print('int to float')
for ind in [10, 5, 0]:
    print(f'float({ind})  {float(ind)}')

print('\nfloat to int')
for flt in [1.1, 0.5, 0.0]:
    print(f'int({flt})  {int(flt)}')
