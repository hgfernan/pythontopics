#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code of the conversion to Boolean values

Created on Sat Jun 10 17:18:47 2023

@author: hilton
"""

print('int to bool')
for ind in [10, 5, 0]:
    print(f'bool({ind})  {bool(ind)}')

print('\nfloat to bool')
for flt in [1.1, 0.5, 0.0]:
    print(f'bool({flt})  {bool(flt)}')

print('\nstr to bool')
for s in ['ab', 'a', '']:
    print(f'bool(\'{s}\')  {bool(s)}')

print('\nlist to bool')
for s in [[1, 2], [1], []]:
    print(f'bool({s})  {bool(s)}')

print('\nNone to bool')
print(f'bool({None})  {bool(None)}')
