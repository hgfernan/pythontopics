#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 18 12:49:39 2023

@author: hilton
"""

s = 'abcdefgh'
print(s)

s = "abcdefgh"
print(s)

print(f's {s}')
fmt = f's {s}'
print(fmt)

print('String básica')
print(f's {s}')
print(f's "{s}"')
print(f"s '{s}'")

print('')
print('Strings são sequências')
print(f's[0] {s[0]}')

print('')
print('Strings são sequências imutáveis')
# s[0] = 'A'

print('')
print('Uso de caracteres de escape para aspas')
print(f's \'{s}\'')
print(f"s \"{s}\"")

print('')
print('Uso desnecessário de caracteres de escape para aspas')
print(f's \"{s}\"')
print(f"s \'{s}\'")

print('')
print('Caracter newline, ou quebra-de-linha')
s1 = 'abcdegfh\nijklmnopq'
print(f's1 {s1}')

print('')
print('Caracteres tab ou tabulador')

s2 = 'abcdefgh\n\tijklmnopq'
print(f's2 {s2}')

print('')
print('Caracter contrabarra')
print('\\')

print('')
print('Caracteres de controle newline e tab de novo')
print('\\n "\n"')
print('\\t "\t"')
print('123456789a')
