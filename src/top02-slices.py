#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Slicing lists and strings

Created on Sat May 27 20:24:46 2023

@author: hilton
"""

s = '0123456789'
l = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

print(f'String original:\n\t\'{s}\'')
print(f'Lista  original:\n\t{l}')

print('\nSlice:')
print(f'String:\n\t\'{s[0:3]}\'')
print(f'Lista:\n\t{l[0:3]}')

print('\nSlice sem parada:')
print(f'String:\n\t\'{s[3:]}\'')
print(f'Lista:\n\t{l[3:]}')

print('\nSlice sem início:')
print(f'String:\n\t\'{s[:3]}\'')
print(f'Lista:\n\t{l[:3]}')

print('\nSlice começando de 1, terminando em 3:')
print(f'String:\n\t\'{s[1:3]}\'')
print(f'Lista:\n\t{l[1:3]}')

print('\nSlice indo até -1:')
print(f'Lista:\n\t{l[:-1]}')

print('\nSlice começando em -1:')
print(f'Lista:\n\t{l[-1:]}')

print('\nSlice indo até -2:')
print(f'Lista:\n\t{l[:-2]}')

print('\nPasso padrão')
print(f'Lista:\n\t{l[0 : 3 : 1]}')

print('\nSlice indo até -1, passo 2:')
print(f'Lista:\n\t{l[:-1:2]}')

print('\nToda lista, passo -1:')
print(f'Lista:\n\t{l[::-1]}')

