#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code of the conversion from strings to other data types

Created on Wed Jun 21 21:09:33 2023

@author: hilton
"""

print('string to bool')
for s_truth in ['False', 'True']:
    truth = bool(s_truth)
    print(f'The string \'{s_truth}\' is ' +\
          f'converted to the bool {truth}')

print('\nstring to int')
s_sold_no = '15'
sold_no = int(s_sold_no)
print(f'The string \'{s_sold_no}\' is ' +\
      f'converted to the int {sold_no}')

print('\nstring to float')
s_btcusd = '29956.62'
btcusd = float(s_btcusd)
print(f'The string \'{s_btcusd}\' is ' +\
      f'converted to the float {btcusd}')
