#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Estimating the precision of the machine
Created on Wed Jun 14 20:33:43 2023

@author: hilton
"""

import math # log10()
import sys  # float_info

print(f'Standard eps {sys.float_info.epsilon}')

count = 0
eps = 1.0
new_eps = eps

while (1.0 + new_eps) > 1.0:
    eps = new_eps
    new_eps /= 2
    count += 1 
    
print(f'eps    {eps}')
print(f'count  {count}')

digits = -int(math.log10(eps))
print(f'digits {digits}')

s = str(1.0 + eps)

print('\nOne plus eps')
print(s)

last_zero = s.rfind('0')
print(last_zero*'-' + '^')

print(f'\nLast zero {last_zero}')
print(f'Digits that are significant {last_zero - 1}')
