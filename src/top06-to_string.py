#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code of the conversion of other data types to strings

Created on Wed Jun 21 16:40:39 2023

@author: hilton
"""

print('int to string')
sold_no = 15
explic_1 = 'Explicit: Hello, soldier number ' +\
    str(sold_no)
print(explic_1)

print(f'Implicit 1: Hello, soldier number {sold_no}') 

print('Implicit 2: ', end='')
print(sold_no)

print('\nfloat to string')
interest = 11.3
explic_1 = 'Explicit: The interest rate is ' +\
    str(interest)
print(explic_1)

print(f'Implicit 1: The interest rate is {interest}') 

print('Implicit 2: ', end='')
print(interest)

print('\nbool to string')
truth = True
explic_1 = 'Explicit: It is ' + str(truth)
print(explic_1)
