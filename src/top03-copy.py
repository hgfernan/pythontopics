#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copy and change of slice

Created on Wed May 31 19:29:31 2023

@author: hilton
"""

l = [0, 1, 2, 3, 4, 5]
print(f'Original\n\tl {l}')

l_copy = l

l0_sav = l[0]
l[0] = 15
print(f'\nChanged l[0] from {l0_sav} to {l[0]}')

print('\nAfter attribution')
print(f'\tl      = {l}')
print(f'\tl_copy = {l_copy}')

print(f'\nl == l_copy: {l == l_copy}')

l[0] = l0_sav

print('\nWill copy ')
l_copy = l[:]
l[0] = 15

print(f'\nChanged l[0] from {l0_sav} to {l[0]} again')

print(f'\nl == l_copy: {l == l_copy}')

print('\nAfter duplication')
print(f'\tl      = {l}')
print(f'\tl_copy = {l_copy}')
