#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code of the conversion from bool values to other data types

Created on Wed Jun 21 22:10:46 2023

@author: hilton
"""

print('bool to string')
for truth in [False, True]:
    s_truth = str(truth)
    print(f'The bool {truth} is ' +\
          f'converted to the string \'{s_truth}\'')

print('\nbool to int')
for truth in [False, True]:
    i_truth = int(truth)
    print(f'The bool {truth} is ' +\
          f'converted to the int {i_truth}')

print('\nbool to float')
for truth in [False, True]:
    f_truth = float(truth)
    print(f'The bool {truth} is ' +\
          f'converted to the float {f_truth}')
