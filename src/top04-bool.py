#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sample code of basic operations with Boolean values

Created on Mon Jun  5 15:39:23 2023

@author: hilton
"""

print('\nCondição')
b1 = True
if b1: 
    print('True')

else: 
    print('False')

print('\nRepetição')    
i = 8
b1 = i < 10
print(f'Before loop: i {i} b1 {b1}')

while b1:
    i += 1
    b1 = i < 10 
    print(f'i {i} b1 {b1}')
    
print(f'After  loop: i {i} b1 {b1}')

print('\nOrdem total')
b1 = i < 10
print(f'{i} < 10: {b1}')

b1 = i > 10
print(f'{i} > 10: {b1}')

b1 = i == 10
print(f'{i} == 10: {b1}')

if i < 10: 
    print(f'{i} is Smaller than 10')

elif i > 10: 
    print(f'{i} is Greater than 10')

else:
    print(f'{i} is Equal to 10')
    
print('\nTruth tables')

print('\nb1\tnot b1')
for b1 in [False, True]: 
    print(f'{b1}\t{not b1}')

print('\nb1\tb2')
for b1 in [False, True]: 
    for b2 in [False, True]:
        print(f'{b1}\t{b2}')

print('\nb1\tb2\tb1 and b2')
for b1 in [False, True]: 
    for b2 in [False, True]:
        print(f'{b1}\t{b2}\t{b1 and b2}')

print('\nb1\tb2\tb1 or b2')
for b1 in [False, True]: 
    for b2 in [False, True]:
        print(f'{b1}\t{b2}\t{b1 or b2}')
